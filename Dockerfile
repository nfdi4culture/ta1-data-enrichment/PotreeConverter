FROM node:18
WORKDIR /app
COPY . /app

RUN apt-get update
RUN apt-get install -y --no-install-recommends python3 python3-pip
RUN pip3 install fastapi
RUN pip3 install uvicorn

RUN apt-get install -y --no-install-recommends cmake libtbb-dev build-essential
        
RUN cmake . -B ./build
RUN cmake --build ./build --config Release;
RUN cmake --install ./build
        
RUN apt-get remove -y cmake
RUN apt-get autoremove -y
RUN apt-get autoclean
RUN rm -rf /var/lib/apt/lists/*

EXPOSE 7999

CMD ["python3", "/app/Api/main.py"]