from fastapi import FastAPI
import uvicorn
import subprocess
import os
import json


app = FastAPI()


@app.get("/{object_id}/{object_name}")
def startPoTreeProcess(
        object_id: str,
        object_name: str):
    os.chdir(f"/app/uploads/cloud/{object_id}/")
    subprocess.call(["pwd"])
    #state = subprocess.call(["PotreeConverter", f"./{object_name}/", "-o ."])
    state = 0
    if (state == 0):
        progress = open("progress.json", "w")
        finished = '{"progress": 100,"finished": true}'
        progress.write(finished)
    return finished


@app.get("/process/{id}")
def getProgress(id: int):
    return 1
if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", port=7999, reload=True)
